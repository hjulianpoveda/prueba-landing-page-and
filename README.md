<h1> Prueba Landing Page AND</h1>

<p>Prueba de conocimientos en web basados en el desarrollo Landing page previamente diagramado.</p>

<h3>Insumos</h3>

<h3>Tecnologías Implementadas</h3>
    <ul>
        <li>HTML5</li>
        <li>CSS3</li>
    </ul>

<h3>Frameworks Utilizados</h3>
    <p>Cargados desde sus respectivos CDN</p>
    <ul>
        <li>BOOTSTRAP 4</li>
        <li>JQUERY 3.3.1 (Requerido por Bootstrap, no utilizado)</li>
    </ul>

<h3>Metodología usada</h3>
<p>Mobile first</p> 

<h4>Puntos de Responsive</h4>
    <p>Se estableció un máximo crítico para evitar pixelación en las imágenes</p>
    <ul>
        <li>1400 px</li>
        <li>1200 px</li>
        <li>990 px</li>
        <li>768 px</li>
    </ul>

<h3>Tipografías Usadas</h3>
<p>Cargadas desde el CDN de Google web fonts</p>
    <ul>
        <li>Dancing Script (Sólo glifos usados en el logo)</li>
        <li>Poppins</li>
        <li>Lato</li>
    </ul>
    
<h4>Tipos de imágenes</h4>
<p>Todas las imágenes utilizadas están en formato .png</p>


<h4>Menú de navegación</h4>
    <p>Botón oculto de menú generado por defecto de bootstrap</p>

<h4>Notas finales</h4>
<p>Se indexó un archivo de scripts vacío en caso de que fuera necesario para la prueba, no se requirió</p>
<p>

